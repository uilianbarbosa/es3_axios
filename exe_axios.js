import axios from 'axios';

class Api {

  static async getTemp(cid) {
    const response = new Filtro((await axios.get(`https://api.hgbrasil.com/weather?key=285c7a5a&city_name=${cid}`)).data.results)
    return response
  }
}

class Filtro {
  constructor({ city_name, description, temp, condition_slug, humidity, currently }) {
    this.city_name = city_name,
      this.description = description,
      this.temperature = temp,
      this.humidity = humidity,
      this.currently = currently
  }
}
Api.getTemp('Pelotas, RS').then(Filtro => { console.log(Filtro) })